var nforce = require("./login");
var fs = require('fs');
var queries = JSON.parse(fs.readFileSync(__dirname + "/queries.json"));

if(process.argv.length < 3) {
    console.log("usage: node runquery <queryname>");
    console.log("valid queries are:\n\t" + Object.keys(queries).join("\n\t"));
    process.exit();
}
var queryname = process.argv[2];
console.log("using query: " + queryname);

nforce.connect(function(err, connectionDetails) {
    if(err) {throw err;}
    var sf = connectionDetails.nforce;
    var oauth = connectionDetails.oauth;
    sf.query(queries[queryname] || queryname, oauth, function(err, results) {
        if(err) {
            throw err;
        }
        if(results && results.records) {
            console.log(JSON.stringify(results.records, null, 2));
        } else {
            console.log("no results");
        }
    });
});
