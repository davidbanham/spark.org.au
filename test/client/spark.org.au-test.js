/*globals Spark:false */
"use strict";

(function($) {

  module('Spark', {
    setup: function() {
      this.elems = $('#qunit-fixture').children();
    }
  });

  test('is awesome', 1, function() {
    strictEqual(Spark, 'awesome', 'Spark is awesome');
  });

}(jQuery));
