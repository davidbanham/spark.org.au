var config = require("../config");
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var LocalStrategy = require('passport-local').Strategy;
var db = require("../lib/db");
var hasher = require("../lib/hasher");
var _ = require("lodash");

var authOpts = {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true
};

passport.serializeUser(function(user, callback) {
    "use strict";
    callback(null, user._id);
});

passport.deserializeUser(function(id, callback) {
    "use strict";
    db.ready(function(api) {
        api.user.get(id, function(err, user) {
            if(err) {
                if (err.message === 'missing') {
                    console.log('Attempt to log in as unknown user ' + id);
                    return callback(null, null);
                }
                console.log('Error while authenticating ' + id);
                return callback(err);
            }
            callback(null, user);
        });
    });
});

// login with a username and password
function loginWithLocal(username, password, callback) {
    "use strict";
    db.ready(function(api) {
        api.user.getByAuth("local", username, function(err, user) {
            if(err) {
                return callback(err);
            }
            if(!user) {
                return callback(null, false, { message: 'Incorrect username.' });
            }
            var auth = _.find(user.authentication, { provider: 'local' });
            var hashed = hasher.computeWithSalt(password, auth.iterations, auth.salt);
            if(auth.password !== hashed.hash) {
                return callback(null, false, { message: 'Incorrect password.' });
            }
            callback(null, user);
        });
    });
}
passport.use(new LocalStrategy(loginWithLocal));

var facebookEnabled = !!config.facebook.clientID;
function loginWithFacebook(accessToken, refreshToken, profile, callback) {
    "use strict";
    db.ready(function(api) {
        api.user.getByAuth("facebook", profile.id, function(err, user) {
            if(err) {
                return callback(err);
            }
            if(user) {
                return callback(null, user);
            }
            var newUser = api.user.blank();
            if(profile.name) {
                newUser.first_name = profile.name.givenName;
                newUser.last_name = profile.name.familyName;
            } else {
                newUser.first_name = profile.display_name;
            }
            if(profile.emails && profile.emails.length) {
                newUser.emails = profile.emails[0];
            }
            newUser.is_registered = true;
            newUser.authentication.push({
                provider: 'facebook',
                id: profile.id,
                oauth_token: accessToken,
                oauth_refresh_token: refreshToken
            });
            api.user.save(newUser, function(err, savedUser) {
                if(err){ return callback(err); }
                callback(null, savedUser);
            });
        });
    });
}
if(facebookEnabled) {
    passport.use(new FacebookStrategy(config.facebook, loginWithFacebook));
}

module.exports.addAuthRoutes = function(app) {
    "use strict";
    app.post('/auth/login', passport.authenticate('local', authOpts));
    if(facebookEnabled) {
        app.get('/auth/facebook', passport.authenticate('facebook'));
        app.get('/auth/facebook/callback', function(req, res, next) {
            // passport seems outdated - facebook is returning error_code and error_message
            // but passport is looking for an "error" param
            if (req.query && !req.query.error && req.query.error_code) {
                req.query.error = true;
            }
            next();
        }, passport.authenticate('facebook', authOpts));
    }
    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/');
    });
};
