
/*
 * GET users listing.
 */

function userProfile(req, res) {
    "use strict";
    res.send("/userProfile/:userId");
}
module.exports.userProfile = userProfile;