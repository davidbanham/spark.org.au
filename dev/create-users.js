var db = require("../lib/db");
var hasher = require('../lib/hasher');

db.ready(function(api) {
    var user = api.user.blank();
    user.first_name = "Adam";
    user.last_name = "Ahmed";
    user.email = "hitsthings@example.com";
    user.is_registered = true;

    var hashed = hasher.compute('adam');
    user.authentication.push({
        provider: 'local',
        id: 'adam',
        password: hashed.hash,
        salt: hashed.salt,
        iterations: hashed.iterations
    });
    api.user.save(user, function(err, savedUser) {
        if (err) throw err;

        console.log('Created %j', savedUser);
    });
});
