var crypto = require('crypto');
var config = require('../config');

function computeWithSalt(value, iterations, salt) {
	"use strict";
	var hash = new Buffer(value + salt + config.hashingSecret, 'utf8');
	for(var i=0; i<iterations; i++) {
		hash = new Buffer(crypto.createHash('sha256').update(hash).digest('binary'), 'binary');
	}
	return {
		hash: hash.toString('base64'),
		iterations: iterations,
		salt: salt
	};
}

var allowedChars = "abcdefg0hijklm1nopq6rstuvwx2yzABCD7EFGHI3JKLMN8OPQ4RSTU5VW9XYZ0";
var allowedCharsLen = allowedChars.length;
function createRandomSalt() {
	"use strict";
	var salt = "";
	for(var i=0;i<8;i++){
		salt += allowedChars[Math.floor(Math.random() * allowedCharsLen)];
	}
	return salt;
}

function compute(value) {
	"use strict";
	var iterations = Math.floor(Math.random() * 20);
	var salt = createRandomSalt();
	return computeWithSalt(value, iterations, salt);
}


module.exports.computeWithSalt = computeWithSalt;
module.exports.compute = compute;
module.exports.createRandomSalt = createRandomSalt;
