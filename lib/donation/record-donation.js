"use strict";
var _ = require('lodash');
var db = require('../db');
var projectHelper = require('../project-helper');

function addContribution(grant, transaction, user, is_anonymous, receipt_emailed) {
    var transactionDate = Date.now();
    var transactionAmount = Number(transaction.amount);
    grant.received_usd += transactionAmount;
    grant.contributions.push({
        user_id : user._id,
        is_anonymous : is_anonymous,
        receipt_emailed : receipt_emailed,
        amount_usd : transactionAmount,
        transaction_date : transactionDate,
        transaction : transaction
    });

    if (grant.amount_usd <= grant.received_usd) {
        grant.attained_date = transactionDate;
    }
}

function getGrantInfo(grant) {
    return {
        goal_usd : grant.amount_usd,
        attained_usd : grant.received_usd
    };
}

function recordTransaction(user, project, transaction, isAnonymous, followProject, receiptEmailed, next) {
    db.ready(function(api) {

        if (followProject && !~project.followers.indexOf(user._id)) {
            project.followers.push(user._id);
        }

        var grant = projectHelper.getCurrentGrant(project);
        if (!grant) {
            return next({
                message : 'No grants found',
                user : user,
                project : project,
                transaction : transaction
            });
        }
        addContribution(grant, transaction, user, isAnonymous, receiptEmailed);

        var grantinfo = getGrantInfo(grant);

        api.project.save(project, function(err, project) {
            if (err) {
                // conflict => reread and retry
                if (err.message === 'conflict') {
                    api.project.get(project.slug, function(err, foundProject) {
                        if (err) {
                            return next(err);
                        }
                        if (!foundProject) {
                            // we're totally fucked.
                            return next({
                                message : 'Project not found after transaction ' +
                                                transaction.id + ' succeeded.',
                                user : user,
                                project : project,
                                transaction : transaction
                            });
                        }
                        recordTransaction(user, foundProject, transaction, isAnonymous, next);
                    });
                }

                // missing ? => was deleted!! dunno what to do about that
                return next(err);
            }
            next(null, grantinfo);
        });
    });
}

module.exports = recordTransaction;
