var createSchema = require('json-gate').createSchema;
var fs = require("fs");

function getSchema(name) {
	"use strict";
    var txt = fs.readFileSync("../doc/db_schema/" + name + ".json");
    var json = JSON.parse(txt);
    return createSchema(json);
}

module.exports = {
    accelerator: getSchema("accelerator"),
    changemaker: getSchema("changemaker"),
    project: getSchema("project"),
    user: getSchema("user")
};
