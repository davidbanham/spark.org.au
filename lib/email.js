/*emailSmtp
 * The purpose of this file is to abstract the email package (nodemailer) and
 * present a simple interface for sending JIT emails on certain user interactions
 * from controllers
 */

var config = require("../config.js");
var nodemailer = require("nodemailer");

var isConfigured = config.emailSmtp.fromAddress;

// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP", config.emailSmtp);

function sendMail(options, callback) {
    // send mail with defined transport object
    smtpTransport.sendMail(options, callback);
}

module.exports.sendMail = isConfigured ? sendMail : function(options, callback) {
    console.log('SMTP not configured. Did not send email.', options);
    callback();
};
