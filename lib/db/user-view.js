/*jshint strict:false*/
/*globals emit,reduce*/
module.exports = {
    _id: "user",
    language: "javascript",
    views: {
        byAuth: {
            map: function(doc) {
                if(doc.type === 'user') {
                    for(var i=0, ii=doc.authentication.length; i<ii; i++) {
                        var auth = doc.authentication[i];
                        emit([auth.provider, auth.id], doc);
                    }
                }
            }
        },
        byEmail: {
            map: function(doc) {
                if(doc.type === 'user') {
                    emit(doc.email, doc);
                }
            }
        },
        byLastModified: {
            map: function(doc) {
                if(doc.type === 'user') {
                    emit(doc.modified_date, doc);
                }
            }
        }
    }
};