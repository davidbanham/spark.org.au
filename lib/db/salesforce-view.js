    /*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "salesforce",
    language: "javascript",
    views: {
        bySalesforceRefThenType : {
            map: function(doc) {
                emit([ doc.salesforce_ref, doc.type ], null);
            }
        }
    }
};
