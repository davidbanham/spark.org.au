module.exports = function(sparkdb) {
    "use strict";

    function searchChangemakers(filters, callback) {
        var params = { include_docs : true };
        if (filters && filters.accelerator_id) {
            params.key = filters && filters.accelerator_id;
        }
        sparkdb.view('changemaker', 'byAccelerator', params,
            function(err, doc) {
                if (err) {
                    return callback(err);
                }
                callback(null, doc.rows && doc.rows.map(function(row) { return row.doc; }));
            });
    }

    function getChangemaker(id, callback) {
        sparkdb.get(id, callback);
    }

    function insertChangemaker(changemaker, callback) {
        console.log("saving changemaker %j", changemaker);
        function cb(err, body) {
            console.log(body);
            if(err) {
                console.log("error saving changemaker: %j", err);
                return callback(err);
            }
            changemaker._id = body.id;
            changemaker._rev = body.rev;
            callback(null, changemaker);
        }
        if (changemaker._id) {
            sparkdb.insert(changemaker, changemaker._id, cb);
        } else {
            sparkdb.insert(changemaker, cb);
        }
    }

    function updateOrInsertChangeMaker(changemaker, callback) {
        if (!changemaker) {
            return callback(null, null);
        }
        if (!changemaker._id) {
            return insertChangemaker(changemaker, callback);
        }
        var docid = changemaker._id;
        getChangemaker(docid, function(err, cm) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(err && err.message === 'missing') {
                cm = blankChangemaker();
            }
            if(!cm) {
                cm = blankChangemaker();
            }
            var keys = Object.keys(changemaker);
            keys.forEach(function(key) {
                if(key === '_rev') {
                    return;
                }
                cm[key] = changemaker[key];
            });
            insertChangemaker(cm, callback);
        });
    }

    function getCount(callback) {
        sparkdb.view('changemaker', 'count', { group : true }, 
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                callback(null, doc.rows.length && doc.rows[0].value);
            });
    }

    function blankChangemaker() {
        return {
            "type" : "changemaker",
            "salesforce_ref" : "",
            "first_name" : "",
            "last_name" : "",
            "story": "",
            "media" : {}
        };
    }

    return {
        count : getCount,
        search : searchChangemakers,
        get : getChangemaker,
        blank : blankChangemaker,
        upsert: updateOrInsertChangeMaker
    };
};
