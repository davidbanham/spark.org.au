$('.read-full-story, .hide-full-story').click(function(e){
    $(e.currentTarget).hide();
    var name = '#id-' + e.currentTarget.id.substring(5,e.currentTarget.id.length);
    if (e.currentTarget.className == 'read-full-story') {
        $(e.currentTarget.parentElement).find('.hide-full-story').show();
        $(name).show();
    }
    else {
        $(e.currentTarget.parentElement).find('.read-full-story').show();
        $(name).hide();
    }
});

$('.employment, .environment, .gender-equality, .health').click(function(e){
    var  name = e.currentTarget.className;
    $(name).css('display', 'inline');
    $('#read-' + name).css('display', 'none');
    $('#hide-' + name).css('display', 'block');
});